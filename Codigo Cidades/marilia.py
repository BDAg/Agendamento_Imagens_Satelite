#!/c/Python36/python
"""
MODIS API Request
"""
import requests
import time
import os
import cgi
import fiona
import numpy as np
import json
from datetime import datetime,timedelta,date
import pymysql



dateimg = datetime.now()
dateimg -= timedelta(30) #SUBTRAIA AQUI EM DIAS

dataOperation = str(dateimg)[:10].split('-')

ano = dataOperation[0]
mes = dataOperation[1]
dia = dataOperation[2]
#DEFINIR PARÂMETROS DE TAREFA
polyBool = 0 #0- rectangle area, 1-polygon area

startDate = mes + '-' + dia + '-' + ano
endDate = (str(datetime.now().month) + '-' + str(datetime.now().day) + '-' + str(datetime.now().year))
minLat = '-23.2176' #min latitude for desired box region
maxLat = '-21.2176' #max latitude for desired box region
minLon = '-50.9505' #min longitude for desired box region
maxLon = '-48.9505' #max longitude for desired box region
stall_time = 10 #seconds to wait before checking data processing status
dtg = endDate[6:10]+endDate[0:2]

#diretório que contem o shapefiles (CHANGE TO THE LOCATION OF YOUR SHAPEFILES)
data_dir = '/home/carlos/Documentos/Faculdade/agendator/imgs/marilia/'

#diretorio que salva os dados salvos (MUDE O LOCAL ONDE VC VAI BAIXAR AS IMAGENS)
if (polyBool==0):
    out_dir = '/home/carlos/Documentos/Faculdade/agendator/imgs/marilia/'
    area_name = 'Marilia'


#LOGIN (É o login, não o email)
response = requests.post('https://lpdaacsvc.cr.usgs.gov/appeears/api/login', auth=('TesteAppearss', 'Testeappears123'))
token_response = response.json()
print(token_response)
#print(' ')

#CRIAR TAREFA
if (polyBool==0):
    task = {
        'task_type': 'area',
        'task_name': area_name+'_'+dtg,
        'startDate': startDate,
        'endDate': endDate,
        'layer': ['MOD13Q1.006,_250m_16_days_EVI', 'MYD13Q1.006,_250m_16_days_EVI'],
        'bbox': minLon + ',' + minLat + ',' + maxLon + ',' + maxLat,
        'file_type': 'geotiff',
        'projection_name': 'native'
    }


#ENVIAR PEDIDO DE TAREFA
token = token_response['token']
response = requests.post(
    'https://lpdaacsvc.cr.usgs.gov/appeears/api/task',
    params=task,  
    headers={'Authorization': 'Bearer {0}'.format(token)})
task_response = response.json()
task_id = task_response['task_id']
status = task_response['status']
print('Task Request')
print(task_response)
print(' ')

#OBTER STATUS DA TAREFA
inum = 0

while (status!='done' and inum<30):
    print('Data is processing...')
    time.sleep(stall_time) #if not done wait 1 minute and check again
    response = requests.get(
    'https://lpdaacsvc.cr.usgs.gov/appeears/api/status/{0}'.format(task_id), 
    headers={'Authorization': 'Bearer {0}'.format(token)})
    status_response = response.json()
    if 'status' in status_response.keys():
        status = status_response['status']


# DOWNLOAD DATA  
if (status=='done'):
    print('Dados terminados de processar')
    print(' ')
    #INFO PACOTE
   
    response = requests.get('https://lpdaacsvc.cr.usgs.gov/appeears/api/bundle/{0}'.format(task_id))
    bundle_response = response.json()
    fnames_all = bundle_response['files']
    print(len(fnames_all))
    
    # pesquisar por todos os arquivos, mas somente baixar arquivos tif
    for f in fnames_all:
        if f['file_type']=='tif':
            file_id = f['file_id']
            file_name = f['file_name']
            print('Downloading: ' + file_name)
    
            response = requests.get(
                'https://lpdaacsvc.cr.usgs.gov/appeears/api/bundle/{0}/{1}'.format(task_id, file_id),
                stream=True)
            
            #analisar o nome do arquivo do cabeçalho Content-Disposition
            content_disposition = cgi.parse_header(response.headers['Content-Disposition'])[1]
            filename = os.path.basename(content_disposition['filename'])
            
            # criar um diretório de destino para armazenar o arquivo
            filepath = os.path.join(out_dir, filename)
            os.makedirs(os.path.dirname(filepath), exist_ok=True)
            
            # gravar o arquivo no diretório de destino
            with open(filepath, 'wb') as f:
                for data in response.iter_content(chunk_size=8192):
                    f.write(data)

            connection = pymysql.connect(host='us-cdbr-iron-east-01.cleardb.net',
                             db='heroku_1726024d7c2378a',
                             user='b531bde7108aae',
                             password='a43d5a03',
                             )

            cursor = connection.cursor()

            data_atual = date.today()

            pathbd = ('https://agendator.herokuapp.com/imgs/marilia/{}').format(filename)

            # Create a new record
            sql = "INSERT INTO `Imagens` (`cidade`, `data`, `path`) VALUES ('Marilia', '%s', '%s')"%(data_atual, pathbd) 
            cursor.execute(sql)

            # connection is not autocommit by default. So you must commit to save
            # your changes.
            connection.commit()

    print('Todos os arquivos foram baixados')
else:
    print('Dados não foram terminados de ser processados!')

#LOGOUT
token = token_response['token']
response = requests.post(
    'https://lpdaacsvc.cr.usgs.gov/appeears/api/logout', 
    headers={'Authorization': 'Bearer {0}'.format(token)})
if (response.status_code==204):
    print('Logout feito com sucesso')
else:
    print('ERRROR: Logout não feito!')
    
connection.close()
