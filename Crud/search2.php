<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cadastro</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>

	<?php
		if(isset($_POST['ok'])){

		$busca = $_POST['busca'];
		$buscafinal = $_POST['buscafinal'];
		$cidade = $_POST['cidade'];


		//conexão com o banco de dados
		include '../Conexão/conexao.php';


		//codigo sql pra busca pela data e cidade, o codigo pode ter que ser alterado dependendo da estrutura do banco
		$sql = "SELECT * FROM Imagens WHERE data BETWEEN '$busca' and '$buscafinal' and cidade = '$cidade'";
		$res = mysqli_query($con, $sql);
			while($ln = mysqli_fetch_array($res)){
			$img = $ln['path'];
			$data = $ln['data'];
			
			
		//pra mostrar a data com o padrao br e imagem
		echo date('d-m-Y', strtotime($data)).'<br>';	
		echo $cidade.'<br>';
		// echo '<img src='.$img.' width=300px height=300px>';


		?>
		<!-- botao de download -->
		<br><a href="<?php echo $img ?>" download="">Download</a><br>

		<?php }} ?>


	<div class="container-contact100">

	
		<div class="wrap-contact100">


				<form class="contact100-form validate-form" method="post" action="">
					<div class="wrap-input100 validate-input" data-validate = "Digite a data">
						<input class="input100" type="date" name="busca" placeholder="Inclua a data inicial">
						<input class="input100" type="date" name="buscafinal" placeholder="Inclua a data final">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Escola a cidade">
						<select class="input100" name="cidade" type="text">
							<option value="" selected>Selecione</option>
							<option value="Oriente">Oriente</option> 
							<option value="Marilia">Marilia</option>
							<option value="Pompeia">Pompeia</option>
						</select>

						<span class="focus-input100"></span>
					</div>
					
					<div class="container-contact100-form-btn">
						<button type="submit" class="contact100-form-btn" value="buscar" name="ok">
							<span>
								<i aria-hidden="true"></i>
								Buscar Imagens
							</span>
						</button>
					</div>
				</form>
					
				<div class="container-contact100-form-btn">
					<form action ="../FrontEnd/perfil2.php" method="post">
						<button type="submit" class="contact100-form-btn" >
							<span>
								<i  aria-hidden="true"></i>
								Voltar
							</span>
						</button>
					</form>
				</div>
			</form>
		</div>
	</div>



	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>
