# Agendamento de Imagens de Satelite


- [Introdução](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/Introduction)

### Definições

- [Project Charter](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/project-charter)
- [Cronograma](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/cronograma)
- [Equipe](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/Team)
- [Mapa de conhecimento](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/mapa-do-conhecimento)
- [Matriz de habilidade](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/matriz-de-habilidades)

### Documentação da solução

- [MVP](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/minimum-viable-product)
- [Arquitetura da Solução](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/arquitetura-de-solu%C3%A7%C3%A3o)
- [Modelagem de dados](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/modelagem-de-dados)
- [Mapa de definição tecnológica](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/mapa-de-defini%C3%A7%C3%A3o-tecnol%C3%B3gica)
- [Ambiente de desenvolvimento](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/Ambiente-de-Desenvolvimento)
- [Lista de Funcionalidades](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/lista-de-funcionalidades)

### Sprint 1

- [Definição de coleta de dados](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/defini%C3%A7%C3%A3o-de-coleta)
- [Script para Agendamento na Cron](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/script-para-agendamento-na-cron)
- [Documentação do processo de coleta](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/documenta%C3%A7%C3%A3o-processo-de-coleta)
- [Mapeamento do formulário de robo](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/Mapeamento-de-formul%C3%A1rios-para-rob%C3%B4)
- [Testes Web-Bot e Cron](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/relat%C3%B3rio-de-teste-do-web-bot)
- [Relatório Teste](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/relat%C3%B3rio-teste-web-bot)

### Sprint 2

- [Diagrama de telas](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/diagrama-de-tela)
- [Estudo de usuabilidade (UX)](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/experi%C3%AAncia-do-usu%C3%A1rio)
- [Teste de CRUD de usuário](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/teste-de-crud-de-usu%C3%A1rio)
- [Teste de alterar senha](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/Teste-de-alterar-senha)
- [Teste de recuperar senha](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/teste-de-recupera%C3%A7%C3%A3o-de-senha)

### Sprint 3

- [Mockup das Telas de agendamento](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/mockup-de-tela)
- [Estudo de usuabilidade (UX)](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/relat%C3%B3rio-de-usualidade-do-sistema)
- [Teste e validação](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/relat%C3%B3rio-de-testes)
- [Revisão da solução](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/revis%C3%A3o-da-solu%C3%A7%C3%A3o)

### Sprint 4

- [Documentação do ambiente de produção](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/documenta%C3%A7%C3%A3o-ambiente-de-produ%C3%A7%C3%A3o)
- [Teste de ambiente de produção](https://gitlab.com/BDAg/Agendamento_Imagens_Satelite/wikis/Deploy-de-produ%C3%A7%C3%A3o)