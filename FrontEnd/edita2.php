<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	<?php
		session_start();

		$nome = $_SESSION['nome'];
		$email = $_SESSION['email'];
		$id = $_SESSION['logado'];

    ?>
	<?php if(isset($_SESSION['msgErro'])){
			echo '<div class="card-panel red darken-1">
					<span class="white-text">ERRO: '.$_SESSION['msgErro'].'</span>
					</div>';
					
			unset($_SESSION['msgErro']);
		}
		
		if(isset($_SESSION['msgOk'])){
			
			echo '<div class="card-panel green darken-1">
					<span class="white-text">'.$_SESSION['msgOk'].'</span>
					</div>';
					
			unset($_SESSION['msgOk']);
		}
	?>

	<div class="container-contact100">
		
		<div class="wrap-contact100">
		<form method="post" action="../Crud/editar2.php" id="attSenha" >
				
				<span class="contact100-form-title">
					Alterar Dados
				</span>

				<div class="wrap-input100 validate-input" data-validate = "Por favor digite seu e-mail">
					<input class="input100" name="email" type="email" value="<?php echo $email ?>">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Por favor, digite seu nome">
					<input class="input100" name="nome" type="text" value="<?php echo $nome ?>">
					<span class="focus-input100"></span>
				</div>
				
				<input type="hidden" name="id" value="<?php echo $id ?>">

				<div class="container-contact100-form-btn">
					<button type="submit" form="attSenha" class="contact100-form-btn">
						<span>
							<i  aria-hidden="true"></i>
							Alterar Dados	
						</span>
					</button>
				</div>		
			</form>
			<div class="container-contact100-form-btn">
				<form action ="perfil2.php" method="post">
					<button type="submit" class="contact100-form-btn" >
						<span>
							<i  aria-hidden="true"></i>
							Voltar
						</span>
					</button>
				</form>
			</div>
		</div>
	</div>

	
	
	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>
