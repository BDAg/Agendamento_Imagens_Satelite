<?php

session_start();

if ($_SESSION['logado'] == '') {
	header("Location: login2.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cadastro</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>

	<?php
		include "../Conexão/conexao.php";

		echo '<center>';
		
		$email=$_SESSION['email'];
		$nome=$_SESSION['nome'];

		$sql = "SELECT * FROM Usuario WHERE Email = '$email'"; 
	
		$res = $con->query($sql);
		while($ln = mysqli_fetch_array($res)){
			$id = $ln['IdUsuario'];
			$nome = $ln ['Nome'];
			$email = $ln ['Email'];
			$senha = $ln['Senha'];
		}
	?>

	<div class="container-contact100">

	
		<div class="wrap-contact100">
		
				<span class="contact100-form-title">
				<?php			
					echo 'Bem vindo '.$nome.'!!<br><br>';
				?>
				</span>

				<form class="container-contact100-form-btn" id="search" action ="../Crud/search2.php" method="post">
					<input type="hidden" name="id" value="<?php echo $id ?>">
					<button type="submit" class="contact100-form-btn" form="search">
						<span>
							<i aria-hidden="true"></i>
							Buscar Imagens
						</span>
					</button>
				</form>
				
				<form class="container-contact100-form-btn" id="send" action="../FrontEnd/edita2.php" method="post">
					<input type="hidden" name="id" value="<?php echo $id ?>">
					<button type="submit" class="contact100-form-btn" form="send">
						<span>
							<i aria-hidden="true"></i>
							Editar Perfil
						</span>
					</button>
				</form>
				<form class="container-contact100-form-btn" id="senha" action="../FrontEnd/alterarsenha2.php" method="post">
					<input type="hidden" name="id" value="<?php echo $id ?>">
					<button type="submit" class="contact100-form-btn" form="senha">
						<span>
							<i aria-hidden="true"></i>	
							Alterar Senha
						</span>
					</button>
				</form>
				<form class="container-contact100-form-btn" id="deleta" action="../Crud/delete.php" method="post">
					<input type="hidden" name="id" value="<?php echo $id ?>">
					<button type="submit" class="contact100-form-btn" form="deleta">
						<span>
							<i aria-hidden="true"></i>
							Deletar Pefil
						</span>
					</button>
				</form>
				<form class="container-contact100-form-btn"  action="../Crud/sair.php">
					<input type="hidden" name="id" value="<?php echo $id ?>">
					<button type="submit" class="contact100-form-btn" >
						<span>
							<i aria-hidden="true"></i>
							Sair
						</span>
					</button>
				</form>
		</div>
	</div>



	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>
